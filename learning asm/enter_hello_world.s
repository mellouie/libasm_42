; nasm -g -f macho64 -o enter_hello_world.o enter_hello_world.s && ld -macosx_version_min 10.6 -o enter_x -e main enter_hello_world.o

; Les instructions PUSH et POP sont les instructions qui servent à empiler et dépiler les données.
; PUSH registre met le contenu du registre dans la pile (empilement).
; POP registre récupère le contenu de la pile et le stocke dans le registre (dépilage).
; Ainsi, l'instruction PUSH BX empile le contenu du registre BX, et l'instruction POP AX récupère le contenu du sommet de la pile et le transfère dans AX.

%define LEN 256
                ; #define LEN 256

section .bss
    entr resb LEN   ; declare la varible entr et on lui reserve (resb) noctets (LEN)

section .data
    message db 'Your message: '
    message_len equ $-message

    prntmes db 'Your message is: '
    prntmes_len equ $-prntmes

section .text
    global main

    main:
        jmp _message

    _message:
        mov rax, 0x02000004
        mov rdi, 1
        mov rsi, message
        mov rdx, message_len
        syscall
        jmp _read

    _read:
        mov rax, 0x02000003
        mov rdi, 1
        mov rsi, entr           ; a ce point, dans rsi, il y a l'adresse d'entr, soit un espace ou l'on a reserver noctects (nombre d'octets indiques dans rdx) 
        mov rdx, LEN
        syscall
        push rsi                ; met le contenue de rsi dans la stack
        jmp _prntmes

    _prntmes:
        mov rax, 0x02000004
        mov rdi, 1
        mov rsi, prntmes
        mov rdx, prntmes_len
        syscall
        mov rax, 0x02000004
        mov rdi, 1
        pop rsi                 ; recupere le contenu du sommet de la stack (entr qu'on a push plus haut), et le transfere dans rsi (= mov rsi, entr)
        mov rdx, 0x100          ; = 256
        syscall
        jmp exit

    exit:
        mov rax, 0x02000001
        mov rdi, 0
        syscall

