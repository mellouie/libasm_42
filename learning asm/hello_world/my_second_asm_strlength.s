; nasm -f macho64 -o my_second_asm_strlength.o my_second_asm_strlength.s && ld -macosx_version_min 10.6 -o my_second_asm_x -e main my_second_asm_strlength.o

; hello world implementation with the computer to count the string lenght

section .data
hello_world db "Hello world!", 0x0A
hello_world_size equ $ - hello_world
; Pour definir une constante, on utilise l'instruction equ (pour "equal") precédée d'un label et suivi de la
; valeur de la constante : myConstante equ 12345 ATTENTION, une constante n'est pas une variable et,
; similairement aux registres, n'a pas d'adresse a proprement parler : une constante est directement interprétée
; comme une valeur
; $ est l'adresse de la position actuelle avant d'émettre les octets (le cas échéant) pour la ligne sur laquelle il apparaît
; $ - hello_world est comme faire ici - msg, c'est-à-dire la distance en octets entre la position actuelle (à la fin de la chaîne)
; et le début de la chaîne. 

section .text
    global main
main:
        mov rax, 0x02000004         ; SYS_write
        mov rdi, 1                  ; sortie standard
        mov rsi, hello_world        ; buffer avec ma str
        mov rdx, hello_world_size   ; nbyte
        syscall                     ; invoque le kernel

        mov rax, 0x02000001         ; SYS_exit
        mov rdi, 0                  ; exit status, here OK
        syscall                     ; invoque le kernel
