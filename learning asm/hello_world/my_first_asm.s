; Assembling (.s -> .o):
; nasm -f macho64 -o my_first_asm.o my_first_asm.s
; -f macho64 demande au NASM de produire un fichier objet au format Mach-O 64bit.
; Ce devrait être elf64 pour Linux. nasm -hf vous donne une liste des formats pris
; en charge par NASM.
 
; Linking (.o -> executable): 
; ld -macosx_version_min 10.6 -o my_first_asm -e main my_first_asm.o
; -e main indique au linker que main est l'étiquette de notre point d'entrée, et
; le linker peut le trouver parce que nous avons global main dans notre fichier .asm (.s).
; -macosx_version_min est mis pour indiquer la plus ancienne version de Mac OS X sur
; laquelle l'output doit être utilisée. La spécification d'une version ultérieure
; permet linker d'assumer les caractéristiques de ce système d'exploitation dans
; le fichier de sortie. Le format de la version est un numéro de version de Mac OS X
; tel que 10.4 ou 10.5

; pour effectuer un appel système sur un Linux 64-bit, il faut avoir le schéma suivant :
;le registre rax doit contenir le numéro d’appel système ;
;le registre rdi doit contenir le premier argument ;
;le registre rsi doit contenir le deuxième/second argument ;
;le registre rdx doit contenir le troisième argument ;
;le registre r10 doit contenir le quatrième argument ;
;le registre r8 doit contenir le cinquième argument ;
;le registre r9 doit contenir le sixième argument.

; nasm -f macho64 -o my_first_asm.o my_first_asm.s && ld -macosx_version_min 10.6 -o my_first_asm_x -e main my_first_asm.o

section .data                          ; section data est utilisée pour déclarer des données ou des constantes initialisées. Dans cette section, vous pouvez déclarer diverses valeurs de constantes, noms de fichiers, ou taille de tampon, etc.
hello_world db "Hello world!", 0x0A     ; hello_world : définition pour ma chaine. db : « Data Byte », sert à insérer un ou plusieurs octets de données à l'endroit où on les rencontre, au sein du flot d'instructions compilées
                                        ; 0x0a est le caractère de contrôle pour une nouvelle ligne. En C, nous aurions utilisé "\n"

section .text                   ; section text est utilisée pour le code du programme. Cette section doit commencer par la déclaration global _start (main), qui indique au noyau l'entrypoint
        global main             ; fourni un entrypoint à notre programme
main:                           ; main: est un label (étiquette), si dans le programme on fait référence à ce label, on donne la référence à cette adresse
                                ; global main + main: est tout ce dont on a besoin pour assembler et linker
        mov rax, 0x02000004     ; SYS_write | copie l'id du syscall write dans le registre de travail rax
        mov rdi, 1              ; premier argument de write, file descriptor, ici sortie standard stdout 1
        mov rsi, hello_world    ; second argument de write, buffer contenant la string à write | L'assembleur et/ou le linker s'assurera que rsi obtient l'adresse de la chaîne ci-dessus grace au label hello_world
        mov rdx, 13             ; troisième argument de write, copie nbyte of data dans le filedes | La chaîne, avec 0x0A, est longue de 13 bytes
        syscall                 ; invoque le kernel

        mov rax, 0x02000001     ; SYS_exit | mov -> mnémonique pour l'instruction de copier une donnée ; rax (registre 64 bits d'usage générique) est un registre de travail ; ID du syscall d'exit / donc ici : copie l'id du syscall exit dans le registre de travail rax
        mov rdi, 0              ; Process exit value rdi -> répertoire dans lequel passer le premier argument de la fonction ; exit success = 0 
        syscall                 ; invoque le kernel ; syscall est en fait une instruction d'assemblage dédiée qui a été introduite dans le jeu d'instructions x86-64 pour rendre les appels au système d'exploitation plus rapides
                                ; 
                                ; 
                                ; 
                                ; 
                                ; 
                                ; 
                                ; 
                                ; 
                                ; 

