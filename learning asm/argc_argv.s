; print sur la sortie standard le nom du binaire (exe), et de 2 arguments passés
; nasm -g -f macho64 -o argc_argv.o argc_argv.s && ld -macosx_version_min 10.6 -o argc_argv_x -e main argc_argv.o

section .data
    n db 0x0A                       ; n = \n

section .text
    global main
main:
    mov rax, 0x02000004
    mov rdi, 1
    mov rsi, [rsp + 8]              ; rsp : registre stack pointer pointe au début de la stack + 8 -> pointe sur l'adresse de l'executable
    mov rdx, 0xB                    ; nbytes de la taille du nom de mon executable
    syscall
    mov rax, 0x02000004
    mov rdi, 1
    mov rsi, n
    mov rdx, 1
    syscall
    mov rax, 0x02000004
    mov rdi, 1
    mov rsi, [rsp + 16]             ; rsp : registre stack pointer pointe au début de la stack + 16 -> pointe sur l'adresse de mon premier argument (argv[1] en c)
    mov rdx, 3
    syscall
    mov rax, 0x02000004
    mov rdi, 1
    mov rsi, n
    mov rdx, 1
    syscall
    mov rax, 0x02000004
    mov rdi, 1
    mov rsi, [rsp + 24]             ; rsp : registre stack pointer pointe au début de la stack + 24 -> pointe sur l'adresse de mon premier argument (argv[2] en c)
    mov rdx, 3
    syscall
    mov rax, 0x02000004
    mov rdi, 1
    mov rsi, n
    mov rdx, 1
    syscall
    jmp exit

exit:
    mov rax, 0x02000001
    mov rdi, 0
    syscall